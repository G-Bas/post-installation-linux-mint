#!/bin/bash

## Ce script automatise toutes les installations et la configuration après installation de la distributions.
## Le coeur du script est composé de deux partie la première installe qui paquet logiciel et la seconde installe des thèmes,
## des extensions et configurations à la fois communs et spécifiques des différents environnement de bureau.

# Création des différents choix pour la suite

echo -n "Que voulez-vous continuer l'execution du script? ( oui ou non ) "
read reponse1

case "$reponse1" in

oui )  
# Installation update & logiciel du dépôt de base + des dépôts supplémentaires

echo "Mise à jour de la distribution et installation des paquets."

sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt full-upgrade -y

sudo apt install intel-microcode
sudo apt install amd64-microcode
sudo apt install mythes-fr myspell-fr
sudo apt install git
sudo apt install ffmpeg
sudo apt install curl
sudo apt install wget
sudo apt install ufw gufw
sudo apt install gparted
sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-plugins-good
sudo apt install ghostwriter
sudo apt install default-jre default-jre-headless
sudo apt install keepassxc
sudo apt install lsb-base lsb-printing
sudo apt install libsane sane-utils xsltproc
sudo apt install nano
sudo apt install pandoc
sudo apt install lollypop
sudo apt install steam
sudo apt install simple-scan
sudo apt install mint-meta-codecs
sudo apt install texlive-full
sudo apt install thunderbird thunderbird-locale-fr
sudo apt install gtkhash
sudo apt install gimp gimp-help-fr
sudo apt install qpdfview qpdfview-ps-plugin
sudo apt install python3-pydrive deja-dup
sudo apt install timeshift
sudo apt install pdfarranger
sudo apt install gnome-maps
sudo apt install nmap
sudo apt install xclip
sudo apt install soundconverter
sudo apt install celluloid
sudo apt install imagemagick
sudo apt install gnome-system-monitor
sudo apt install skrooge
sudo apt install software-properties-common
sudo apt install fdupes
sudo apt install fdisk
sudo apt install wipe
sudo apt install scrub
sudo apt install partclone
sudo apt install inkscape
sudo apt install redshift-gtk geoclue-2.0

sudo apt install sassc gtk2-engines-murrine gtk2-engines-pixbuf
sudo adduser $USER lp
gufw
sudo systemctl --global disable redshift-gtk
sudo systemctl --global disable redshift
for p in libreoffice rhythmbox ; do sudo apt autoclean $p && sudo apt purge "$p*" ; done
sudo apt install -y flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub net.cozic.joplin_desktop
flatpak install flathub com.spotify.Client
flatpak install flathub com.mojang.Minecraft
flatpak install flathub org.onlyoffice.desktopeditors
flatpak install flathub org.musicbrainz.Picard
flatpak install flathub com.github.johnfactotum.Foliate

# Ajout dépôt VSCodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg 
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 
sudo apt update && sudo apt install -y codium

    # Virtualbox

    echo -n "Voulez-vous installer VirtualBox ? ( oui ou non ) "
    read reponse2 

    case "$reponse2" in

    oui )
        wget -q -O- http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc | sudo apt-key add -
        echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
        sudo apt update
        sudo apt install virtualbox-6.1
    ;;
    non )
        echo "VirtualBox ne sera pas installé"
    ;;
    * )
        echo "Faite un choix valide "
    ;;
    esac

# Balena Etcher
curl -1sLf \
   'https://dl.cloudsmith.io/public/balena/etcher/setup.deb.sh' \
   | sudo -E bash
sudo apt update
sudo apt install balena-etcher-electron

# Dropbox
sudo apt install python3-pip
pip install maestral

#ExpressVPN
firefox https://www.expressvpn.com/fr/latest#linux

# Alias bash personnalisé  

cp bash_aliases.txt ~/.bash_aliases

git clone https://framagit.org/G-Bas/fichier-de-configuration-linux.git
cd fichier-de-configuration-linux
cp redshift.conf.txt ~/.config/redshift.conf

# Couleurs personnalisées de ls
cp ls_colors.txt ~/.ls_colors
dircolors -p > ~/.ls_colors

# Modification du swappiness
sudo bash -c 'echo "vm.swappiness=10" >> /etc/sysctl.conf'
sudo sysctl -p

cd ..

# Nettoyage du dossier

wipe -frie -q fichier-de-configuration-linux

# Ajout de thèmes communs

    mkdir -p ~/.local/share/icons && ln -s ~/.local/share/icons ~/.icons

    wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.icons" sh

    git clone https://github.com/vinceliuice/Jasper-gtk-theme.git
    cd Jasper-gtk-theme
    ./install.sh -d ~/.themes -t {default,red,yellow,orange,blue,green} -c {dark,standard} -s compact --tweaks nord
    cd ..

    git clone https://github.com/vinceliuice/Orchis-theme.git
    cd Orchis-theme/
    ./install.sh -d ~/.themes -t {yellow,red,orange,green,teal,default} -c standard -s compact --tweaks nord primary --round 5px
    cd ..

    git clone https://github.com/vinceliuice/Fluent-icon-theme.git
    cd Fluent-icon-theme/
    ./install.sh -d ~/.icons {standard,green,red,orange}
    cd ..

    git clone https://github.com/vinceliuice/Fluent-gtk-theme.git
    cd Fluent-gtk-theme/
    ./install.sh -d ~/.themes -s compact -t {default,green,red,orange} -c {dark,standard} -i simple --tweaks round
    cd ..

    git clone https://github.com/vinceliuice/Tela-icon-theme.git
    cd Tela-icon-theme/
    ./install.sh -d ~/.icons nord
    cd ..

    git clone https://github.com/vinceliuice/vimix-gtk-themes.git
    cd vimix-gtk-themes/
    ./install.sh -d ~/.themes -t {doder,ruby,beryl,jade} -c {standard,dark} --size standard --tweaks flat

    git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git
    cd Tela-circle-icon-theme/
    ./install.sh -d ~/.icons {blue,manjaro,red,green}
    cd ..

    for d in Jasper-gtk-theme Orchis-theme Tela-circle-theme Fluent-gtk-theme Fluent-icon-theme vimix-gtk-themes ; do wipe -frieq $d ; done

    echo -n "Voulez-vous redémarrer l'ordinateur ? ( oui ou non ) "
    read reponse3 

    case "$reponse3" in

        oui )
        echo "L'ordinateur va redémarrer."
        sudo shutdown -t 10 -r now
        ;;
        non )
        echo "L'ordinateur restera actif."
        ;;
        * )
        echo "Faite un choix valide "
        ;;
        
    esac
;;

non )
echo "Le script n'executera pas aucune installation."
;;

* )
echo "Faite un choix valide "
;;

esac
